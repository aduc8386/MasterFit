package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import com.google.common.base.CharMatcher
import com.google.common.base.CharMatcher.any
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class LoginUseCaseTest {

    private lateinit var userRepository: UserRepository
    private lateinit var sharedPreferenceHelper: SharedPreferenceHelper
    private lateinit var loginUseCase: LoginUseCase

    @Before
    fun setUp() {
        userRepository = mockk()
        sharedPreferenceHelper = mockk{
            every { setUserEmail(any()) } returns mockk()
            every { setUserPassword(any()) } returns mockk()
            every { setIsLoggedIn(any()) } returns mockk()
            every { setIsRemember(any()) } returns mockk()
        }
        loginUseCase = LoginUseCase(userRepository, sharedPreferenceHelper)
    }

    @Test
    fun givenValidEmailAndPassword_whenCallLoginUseCase_returnResponseSuccess() = runTest {
        val email = "aduc8386@gmail.com"
        val password = "123456"

        val expect = ResponseStatus.Success<User>(mockk())

        coEvery {
            userRepository.getUserByEmailAndPassword(
                email,
                password
            )
        } returns expect

        val actual = loginUseCase(email, password)

        verify {
            sharedPreferenceHelper.setUserEmail(email)
            sharedPreferenceHelper.setUserPassword(password)
            sharedPreferenceHelper.setIsLoggedIn(true)
            sharedPreferenceHelper.setIsRemember(true)
        }

        Truth.assertThat(actual).isEqualTo(expect)
    }
}