package com.fpt.ducna40.masterfit.data.repo

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.remote.api.ExerciseMockApiDatasource
import com.fpt.ducna40.masterfit.data.model.Exercise
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class ExerciseMockApiRepositoryTest {

    private lateinit var exerciseMockApiDatasource: ExerciseMockApiDatasource
    private lateinit var exerciseMockApiRepository: ExerciseMockApiRepository

    @Before
    fun setUp() {
        exerciseMockApiDatasource = mockk()
        exerciseMockApiRepository = ExerciseMockApiRepository(exerciseMockApiDatasource)
    }

    @Test
    fun whenCallGetExercises_returnResponseSuccess() = runTest {
        val expect = ResponseStatus.Success<List<Exercise>>(mockk())

        coEvery { exerciseMockApiDatasource.getExercises() } returns expect

        val actual = exerciseMockApiRepository.getExercises()

        Truth.assertThat(actual).isEqualTo(expect)
    }
}