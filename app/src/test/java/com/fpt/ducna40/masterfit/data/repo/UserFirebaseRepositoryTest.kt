package com.fpt.ducna40.masterfit.data.repo

import app.cash.turbine.test
import com.fpt.ducna40.masterfit.data.RegisterRequest
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.remote.firebase.UserFirebaseDatasource
import com.fpt.ducna40.masterfit.data.model.User
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class UserFirebaseRepositoryTest {

    private lateinit var userFirebaseRepository: UserFirebaseRepository
    private lateinit var userFirebaseDatasource: UserFirebaseDatasource

    @Before
    fun setUp() {
        userFirebaseDatasource = mockk()
        userFirebaseRepository = UserFirebaseRepository(userFirebaseDatasource)
    }

    @Test
    fun givenValidInfo_whenCallInsertUserEmailNamePhonePassword_returnResponseSuccess() = runTest(
    ) {

        val validInfo = RegisterRequest(
            fullName = "Nguyen Anh Duc",
            email = "aduc8386@gmail.com",
            phoneNumber = "0359368023",
            password = "123456"
        )

        val expect = ResponseStatus.Error<User>("Empty email")

        coEvery {
            userFirebaseDatasource.insertUserEmailNamePhonePassword(validInfo)
        } returns expect

        val actual = userFirebaseRepository.insertUserEmailNamePhonePassword(validInfo)

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun givenEmptyEmail_whenCallInsertUserEmailNamePhonePassword_returnResponseError() = runTest(
    ) {

        val emptyEmailInfo = RegisterRequest(
            fullName = "Nguyen Anh Duc",
            email = "",
            phoneNumber = "0359368023",
            password = "123456"
        )

        val expect = ResponseStatus.Error<User>("Empty email")

        coEvery {
            userFirebaseDatasource.insertUserEmailNamePhonePassword(emptyEmailInfo)
        } returns expect

        val actual = userFirebaseRepository.insertUserEmailNamePhonePassword(emptyEmailInfo)

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun givenEmptyPassword_whenCallInsertUserEmailNamePhonePassword_returnResponseError() = runTest(
    ) {

        val expect = ResponseStatus.Error<User>("Empty password")

        coEvery {
            userFirebaseDatasource.insertUserEmailNamePhonePassword(
                RegisterRequest(
                    fullName = "Nguyen Anh Duc",
                    email = "aduc8386@gmail.com",
                    phoneNumber = "0359368023",
                    password = ""
                )
            )
        } returns expect

        val actual = userFirebaseRepository.insertUserEmailNamePhonePassword(
            RegisterRequest(
                fullName = "Nguyen Anh Duc",
                email = "aduc8386@gmail.com",
                phoneNumber = "0359368023",
                password = ""
            )
        )

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun givenValidEmailPassword_whenCallGetUserByEmailAndPassword_returnResponseError() = runTest {

        val email = "aduc8386@gmail.com"
        val password = "123456"

        val expect = ResponseStatus.Success<User>(mockk())

        coEvery {
            userFirebaseDatasource.getUserByEmailAndPassword(email, password)
        } returns expect

        val actual = userFirebaseRepository.getUserByEmailAndPassword(email, password)

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun givenEmptyEmail_whenCallGetUserByEmailAndPassword_returnResponseError() = runTest {

        val email = ""
        val password = "123456"

        val expect = ResponseStatus.Error<User>("Empty email")

        coEvery {
            userFirebaseDatasource.getUserByEmailAndPassword(email, password)
        } returns expect

        val actual = userFirebaseRepository.getUserByEmailAndPassword(email, password)

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun whenCallGetCurrentUser_returnFlowUser() = runTest {

        val currentUser = flow<ResponseStatus<User>> {
            emit(ResponseStatus.Success(mockk()))
        }

        coEvery { userFirebaseDatasource.getCurrentUser() } returns currentUser

        userFirebaseRepository.getCurrentUser().test {
            Truth.assertThat(awaitItem()).isInstanceOf(ResponseStatus.Success::class.java)
            awaitComplete()
        }
    }

    @After
    fun tearDown() {

    }
}