package com.fpt.ducna40.masterfit.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.usecase.GetCurrentUserUseCase
import com.fpt.ducna40.masterfit.domain.usecase.LoginUseCase
import com.fpt.ducna40.masterfit.domain.usecase.RegisterUseCase
import com.fpt.ducna40.masterfit.domain.usecase.RegisterWithEmailNamePhonePasswordUseCase
import com.fpt.ducna40.masterfit.domain.usecase.SetGoalUseCase
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class UserViewModelTest {

    @MockK
    private lateinit var loginUseCase: LoginUseCase
    @MockK
    private lateinit var registerUseCase: RegisterUseCase
    @MockK
    private lateinit var registerWithEmailNamePhonePasswordUseCase: RegisterWithEmailNamePhonePasswordUseCase
    @MockK
    private lateinit var getCurrentUserUseCase: GetCurrentUserUseCase
    @MockK
    private lateinit var setGoalUseCase: SetGoalUseCase

    private lateinit var testDispatcherProvider: TestDispatcherProvider
    private lateinit var userViewModel: UserViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        loginUseCase = mockk()
        registerUseCase = mockk()
        registerWithEmailNamePhonePasswordUseCase = mockk()
        getCurrentUserUseCase = mockk()
        setGoalUseCase = mockk()
        testDispatcherProvider = TestDispatcherProvider()
        userViewModel = UserViewModel(
            loginUseCase,
            registerUseCase,
            registerWithEmailNamePhonePasswordUseCase,
            getCurrentUserUseCase,
            setGoalUseCase,
            testDispatcherProvider
        )

    }

    @Test
    fun givenValidEmailAndPassword_whenCallLogin_updateCurrentUser() = runTest {

        val email = "aduc8386@gmail.com"
        val password = "123456"

        val expect = ResponseStatus.Success<User>(mockk())

        coEvery { loginUseCase(email, password) } coAnswers { expect }

        userViewModel.currentUser.test {
            userViewModel.login(email, password)
            Truth.assertThat(awaitItem()).isEqualTo(expect)
        }

    }
}