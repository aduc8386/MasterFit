package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.RegisterRequest
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import javax.inject.Inject

class RegisterWithEmailNamePhonePasswordUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(
        email: String,
        fullName: String,
        phoneNumber: String,
        password: String
    ): ResponseStatus<User> {
        when {
            fullName.isEmpty() -> return ResponseStatus.Error("Name can't be empty")
            phoneNumber.isEmpty() -> return ResponseStatus.Error("Phone number can't be empty")
            email.isEmpty() -> return ResponseStatus.Error("Email can't be empty")
            password.isEmpty() -> return ResponseStatus.Error("Password can't be empty")
            else -> {
                val responseStatus = userRepository.insertUserEmailNamePhonePassword(
                    RegisterRequest(
                        email = email,
                        fullName = fullName,
                        phoneNumber = phoneNumber,
                        password = password
                    )
                )

                if (responseStatus is ResponseStatus.Success) {
                    sharedPreferenceHelper.setUserEmail(email)
                    sharedPreferenceHelper.setUserPassword(password)
                    sharedPreferenceHelper.setUserPhoneNumber(phoneNumber)
                    sharedPreferenceHelper.setUserFullName(fullName)
                }
                return responseStatus
            }
        }
    }

}