package com.fpt.ducna40.masterfit.presentation.component.splash

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.FragmentGetStartedBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment

class GetStartedFragment : BaseFragment<FragmentGetStartedBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_get_started

    override fun bindView() {
        (requireActivity() as MainActivity).hideBottomNavigation()
        binding.btnGetStartedGetStarted.setOnClickListener {
            findNavController().navigate(GetStartedFragmentDirections.actionGetStartedFragmentToOnBoardingFragment())
        }
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}