package com.fpt.ducna40.masterfit.presentation

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel

class MainViewModel : ViewModel() {

    private var _isFirstLaunch = true

    val isFirstLaunch: Boolean
        get() = _isFirstLaunch

    fun setIsNotFirstLaunch() {
        _isFirstLaunch = false
    }

}