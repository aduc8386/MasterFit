package com.fpt.ducna40.masterfit.presentation

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.health.connect.client.HealthConnectClient
import androidx.health.connect.client.PermissionController
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.ActivityMainBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseActivity
import com.fpt.ducna40.masterfit.util.permissionhelper.PermissionHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private lateinit var navController: NavController

    private val userViewModel: UserViewModel by viewModels()
    private val mainViewModel: MainViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.activity_main

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestPermission()

        PermissionHelper(
            this,
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                if (isGranted) {
                    Log.d("ducna", "requestPermissionLauncher: granted")
                } else {
                    Log.d("ducna", "requestPermissionLauncher: not granted")
                }
            }).run {

            requestPermission(
                setOf(
                    Manifest.permission.ACTIVITY_RECOGNITION,
                    Manifest.permission.POST_NOTIFICATIONS,
                )
            )
        }
    }

    override fun bindView() {
        super.bindView()

        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.fcv_main_container
        ) as NavHostFragment
        navController = navHostFragment.navController
        binding.bnvMainNavigation.setupWithNavController(navController)
    }

    fun showBottomNavigation() {
        binding.bnvMainNavigation.visibility = View.VISIBLE
        binding.ivMainSearch.visibility = View.VISIBLE
    }

    fun hideBottomNavigation() {
        binding.bnvMainNavigation.visibility = View.GONE
        binding.ivMainSearch.visibility = View.GONE
    }

    fun showLoading() {
        binding.llcMainLoadingProgress.visibility = View.VISIBLE
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )

    }

    fun hideLoading() {
        binding.llcMainLoadingProgress.visibility = View.GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
//
//    private val requestPermissionLauncher =
//        registerForActivityResult(
//            ActivityResultContracts.RequestPermission()
//        ) { isGranted: Boolean ->
//            if (isGranted) {
//                Log.d("ducna", "requestPermissionLauncher: granted")
//            } else {
//                Log.d("ducna", "requestPermissionLauncher: not granted")
//            }
//        }
//
//    private val PERMISSIONS = setOf(
//        Manifest.permission.ACTIVITY_RECOGNITION,
//        Manifest.permission.POST_NOTIFICATIONS,
//    )
//
//    private fun requestPermission() {
//        for (permission in PERMISSIONS) {
//            when {
//                ContextCompat.checkSelfPermission(
//                    this,
//                    permission
//                ) == PackageManager.PERMISSION_GRANTED -> {
//                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
//                }
//
//                shouldShowRequestPermissionRationale(permission) -> {
//                    requestPermissionLauncher.launch(permission)
//                }
//
//                else -> {
//                    requestPermissionLauncher.launch(permission)
//                }
//            }
//        }
//    }

}