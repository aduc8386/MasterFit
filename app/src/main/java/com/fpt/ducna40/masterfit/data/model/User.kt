package com.fpt.ducna40.masterfit.data.model

import com.google.firebase.database.Exclude
import java.lang.Math.pow
import kotlin.math.pow

data class User(
    val id: String = "",
    val fullName: String = "",
    val phoneNumber: String = "",
    val email: String = "",
    val password: String = "",
    val gender: String? = null,
    val dateOfBirth: String? = null,
    val height: Double = 0.0,
    val weight: Double = 0.0,
    val goal: Goal? = null,

    @Exclude
    @get:Exclude
    @set:Exclude
    var loggings: List<WorkoutLogging>? = null

) {
    @Exclude
    fun isFullInformation() =
        gender != null && dateOfBirth != null && height != 0.0 && weight != 0.0

    @Exclude
    fun getBmi() = weight / ((height / 100) * (height / 100))

    @Exclude
    fun getBmiDesc(): String = when {
        getBmi() < 18.5 -> "You are in the underweight range"
        getBmi() in 18.5..24.9 -> "You are in the healthy weight range"
        getBmi() in 25.0..29.9 -> "You are in the overweight range"
        else -> "You are in the obese range"
    }

}