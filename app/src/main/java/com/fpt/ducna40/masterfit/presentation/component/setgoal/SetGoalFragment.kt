package com.fpt.ducna40.masterfit.presentation.component.setgoal

import android.app.DatePickerDialog
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.FragmentSetGoalBinding
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class SetGoalFragment : BaseFragment<FragmentSetGoalBinding>() {

    private val userViewModel: UserViewModel by activityViewModels()

    override fun getLayoutId(): Int = R.layout.fragment_set_goal

    override fun observeViewModels() {
        lifecycleScope.launch {
            userViewModel.currentUser.collectLatest {
                if (it != null && it.data?.goal != null) {
                    findNavController().navigateUp()
                }
            }
        }
    }

    override fun bindView() {
        (requireActivity() as MainActivity).hideBottomNavigation()
        with(binding) {
            btnSetGoalConfirm.setOnClickListener(this@SetGoalFragment)
            edtSetGoalTargetDate.setOnClickListener(this@SetGoalFragment)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                edtSetGoalTargetDate -> {
                    DatePickerDialog(requireActivity()).run {
                        setOnDateSetListener { _, year, month, dayOfMonth ->
                            val targetDate = "$dayOfMonth/${month + 1}/$year"
                            edtSetGoalTargetDate.setText(targetDate)
                        }
                        show()
                    }
                }

                btnSetGoalConfirm -> {
                    userViewModel.setGoal(
                        edtSetGoalTitle.text.toString(),
                        edtSetGoalTargetDate.text.toString(),
                        edtSetGoalCaloriesTargetWeight.text.toString().toDouble(),
                        edtSetGoalStepsPerDay.text.toString().toInt(),
                        edtSetGoalCaloriesBurnedPerDay.text.toString().toDouble()
                    )
                }
            }
        }
    }

}