package com.fpt.ducna40.masterfit.presentation.customview

import android.content.Context
import android.graphics.*
import android.graphics.Paint.Align
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.toRectF
import com.fpt.ducna40.masterfit.R

class BMIView(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    private val backgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val bmiPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val lightBlue = ContextCompat.getColor(context, R.color.blue_light)
    private val darkBlue = ContextCompat.getColor(context, R.color.blue_dark)
    private val lightPurple = ContextCompat.getColor(context, R.color.purple_light)
    private val darkPurple = ContextCompat.getColor(context, R.color.purple_dark)

    private var width: Int = 0
    private var height: Int = 0

    private var bmi: String = ""
    private var bmiDesc: String = ""

    fun setBmi(b: String) {
        bmi = b
        invalidate()
    }
    fun setBmiDesc(bd: String) {
        bmiDesc = bd
        invalidate()
    }

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.BMIView,
            0,
            0
        ).apply {
            try {
                bmi = getString(R.styleable.BMIView_bmi) ?: ""
                bmiDesc = getString(R.styleable.BMIView_bmiDesc) ?: ""
            } finally {
                recycle()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        width = MeasureSpec.getSize(widthMeasureSpec)
        height = MeasureSpec.getSize(heightMeasureSpec)

        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawBackground(canvas)

        drawTitle(canvas)

        drawSubTitle(canvas)

        drawButton(canvas)

        drawBMI(canvas)

    }

    private fun drawBMI(canvas: Canvas) {

        val x = (width * 0.8).toFloat()
        val y = (height * 0.5).toFloat()
        val radius = (height * 0.3).toFloat()

        bmiPaint.apply {
            color = Color.WHITE
        }

        canvas.drawCircle(
            x, y, radius, bmiPaint
        )

        val rectF = RectF(x - radius - 30, y - radius - 30, x + radius + 30, y + radius + 30)

        backgroundPaint.apply {
            shader = LinearGradient(
                rectF.left,
                rectF.top + radius + 30,
                rectF.right,
                rectF.top + radius + 30,
                lightPurple,
                darkPurple,
                Shader.TileMode.CLAMP
            )
            setShadowLayer(4.toDp().toFloat(), 0f, 0f, lightPurple)
        }

        canvas.drawArc(rectF, -108f, 108f, true, backgroundPaint)

        drawText(
            canvas = canvas,
            text = bmi,
            size = 12,
            typeFace = Typeface.create(Typeface.DEFAULT, Typeface.BOLD),
            align = Align.CENTER,
            x = x + radius / 2,
            y = y - radius / 2
        )
    }

    private fun drawButton(canvas: Canvas) {

        val buttonWidth = (width * 0.3).toFloat()
        val buttonHeight = (height * 0.25).toFloat()

        val x0 = (width * 0.05).toFloat()
        val y0 = (height * 0.6).toFloat()
        val x1 = x0 + buttonWidth
        val y1 = y0 + buttonHeight

        val rectF = RectF(
            x0, y0, x1, y1
        )

        backgroundPaint.apply {
            shader = LinearGradient(
                x0, y0, x1, y1, lightPurple, darkPurple, Shader.TileMode.CLAMP
            )
            setShadowLayer(4.toDp().toFloat(), 0f, 0f, lightPurple)
        }

        canvas.drawRoundRect(
            rectF, 32.toDp().toFloat(), 32.toDp().toFloat(), backgroundPaint
        )

        val bounds = Rect()
        textPaint.getTextBounds("View More", 0, "View More".length, bounds)
        val height = bounds.height()

        drawText(
            canvas = canvas,
            text = "View More",
            size = 10,
            typeFace = Typeface.create(Typeface.DEFAULT, Typeface.BOLD),
            align = Align.CENTER,
            x = x0 + buttonWidth / 2,
            y = y0 + buttonHeight / 2 + height / 2
        )
    }

    private fun drawSubTitle(canvas: Canvas) {
        drawText(
            canvas = canvas,
            text = bmiDesc,
            size = 12,
            typeFace = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL),
            x = (width * 0.05).toFloat(),
            y = (height * 0.45).toFloat()
        )
    }

    private fun drawTitle(canvas: Canvas) {
        drawText(
            canvas = canvas,
            text = "BMI (Body Mass Index)",
            size = 14,
            typeFace = Typeface.create(Typeface.DEFAULT, Typeface.BOLD),
            x = (width * 0.05).toFloat(),
            y = (height * 0.3).toFloat()
        )
    }

    private fun drawBackground(canvas: Canvas) {
        val rect = Rect(0, 0, width, height)

        backgroundPaint.apply {
            shader = LinearGradient(
                0f, height.toFloat(), width.toFloat(), 0f, lightBlue, darkBlue, Shader.TileMode.CLAMP
            )
            setShadowLayer(elevation, 0f, 0f, lightBlue)
        }

        val backgroundDots =
            ResourcesCompat.getDrawable(resources, R.drawable.bg_background_dots, null)!!.toBitmap(
                width.toDp(), height.toDp()
            )

        canvas.drawRoundRect(
            rect.toRectF(), 24.toDp().toFloat(), 24.toDp().toFloat(), backgroundPaint
        )
        canvas.drawBitmap(backgroundDots, null, rect, backgroundPaint)

    }

    private fun drawText(
        canvas: Canvas,
        text: String,
        size: Int,
        align: Align = Align.LEFT,
        typeFace: Typeface,
        x: Float,
        y: Float
    ) {
        textPaint.apply {
            textAlign = align
            textSize = size * resources.displayMetrics.density
            color = Color.WHITE
            typeface = typeFace
        }
        canvas.drawText(
            text, x, y, textPaint
        )
    }

    private fun Int.toDp(): Int {
        return (this * resources.displayMetrics.density + 0.5f).toInt()
    }
}