package com.fpt.ducna40.masterfit.presentation.customview

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.Shader
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.toRectF
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging

class ActivityChartView(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    private val backgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val columnPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val columnBackgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val lightBlue = ContextCompat.getColor(context, R.color.blue_light)
    private val darkBlue = ContextCompat.getColor(context, R.color.blue_dark)

    private val lightPurple = ContextCompat.getColor(context, R.color.purple_light)
    private val darkPurple = ContextCompat.getColor(context, R.color.purple_dark)
    private val black10 = ContextCompat.getColor(context, R.color.black_10)
    private val gray = ContextCompat.getColor(context, R.color.gray_empress)

    private val progressLengthAnimator: Animator
    private var progressLength: Float = 0f

    private var width: Int = 0
    private var height: Int = 0
    private var columnHeight: Int = 0

    private var workoutLoggings = listOf<WorkoutLogging>()

    private var goal: Goal? = null

    fun setData(loggings: List<WorkoutLogging>, g: Goal) {
        workoutLoggings = loggings
        goal = g
        requestLayout()
        invalidate()
    }

    init {
        progressLengthAnimator = AnimatorInflater.loadAnimator(
            context,
            R.animator.animator_progress_length
        ) as ValueAnimator

        progressLengthAnimator.addUpdateListener {
            progressLength = (it.animatedValue as Float).toFloat()
            Log.d("ducna", "update value in line 34: ${it.animatedValue}")
            invalidate()
        }

        progressLengthAnimator.start()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        width = MeasureSpec.getSize(widthMeasureSpec)
        height = MeasureSpec.getSize(heightMeasureSpec)
        columnHeight = (0.65 * height).toInt()

        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawBackground(canvas)

        workoutLoggings.forEach { workoutLogging ->
            Log.d("ducna", "onDraw: $workoutLogging")
            goal?.let { drawColumn(canvas, workoutLogging, it) }
        }
    }

    private fun drawBackground(canvas: Canvas) {
        val rect = Rect(0, 0, width, height)

        backgroundPaint.apply {
            color = Color.WHITE
        }

        canvas.drawRoundRect(
            rect.toRectF(), 24.toDp().toFloat(), 24.toDp().toFloat(), backgroundPaint
        )
    }

    private fun drawColumn(canvas: Canvas, workoutLogging: WorkoutLogging, goal: Goal) {

        val index = workoutLoggings.indexOf(workoutLogging)

        val startingBottom = height * 0.8f
        val startingStart = width * (1 / 15f)
        val columnWidth = width * (2 / 15f)
        val step = index * width * (2 / 15f)
        val maxValue = height * 0.1f
        val minValue = height * 0.8f
        val columnLength = minValue - maxValue

        val percent: Float = if (workoutLogging.currentStep / goal.stepsPerDay >= 1) {
            1f
        } else {
            (workoutLogging.currentStep.toFloat() / goal.stepsPerDay)
        }

        val columnHeight = columnLength * percent

        val columnBackground = RectF(
            startingStart + step,
            startingBottom,
            columnWidth + step,
            maxValue
        )

        columnBackgroundPaint.apply {
            color = black10
            style = Paint.Style.FILL
        }

        canvas.drawRoundRect(columnBackground, 32f, 32f, columnBackgroundPaint)

        val column = RectF(
            startingStart + step,
            startingBottom,
            columnWidth + step,
            startingBottom - columnHeight * progressLength
        )

        columnPaint.apply {
            color = Color.WHITE
            shader = if (index % 2 == 0) LinearGradient(
                startingStart + step,
                (height * 0.65f) / 2,
                columnWidth + step,
                (height * 0.65f) / 2,
                lightPurple,
                darkPurple,
                Shader.TileMode.CLAMP
            ) else LinearGradient(
                startingStart + step,
                (height * 0.65f) / 2,
                columnWidth + step,
                (height * 0.65f) / 2,
                lightBlue,
                darkBlue,
                Shader.TileMode.CLAMP
            )
        }

        canvas.drawRoundRect(column, 32f, 32f, columnPaint)

        drawText(
            canvas = canvas,
            text = workoutLogging.date,
            textColor = gray,
            size = 12,
            align = Paint.Align.CENTER,
            x = startingStart + step + (columnWidth) / 4,
            y = startingBottom * 1.1f
        )

    }

    private fun drawText(
        canvas: Canvas,
        text: String,
        textColor: Int,
        size: Int,
        align: Paint.Align,
        x: Float,
        y: Float
    ) {
        textPaint.apply {
            textAlign = align
            textSize = size * resources.displayMetrics.density
            color = textColor
        }
        canvas.drawText(
            text, x, y, textPaint
        )
    }

    private fun Int.toDp(): Int {
        return (this * resources.displayMetrics.density + 0.5f).toInt()
    }

}