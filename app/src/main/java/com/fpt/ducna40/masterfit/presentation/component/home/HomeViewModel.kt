package com.fpt.ducna40.masterfit.presentation.component.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Exercise
import com.fpt.ducna40.masterfit.domain.usecase.GetCurrentStepsUseCase
import com.fpt.ducna40.masterfit.domain.usecase.GetExercisesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getExercisesUseCase: GetExercisesUseCase,
) : ViewModel() {

    private var _exercises =
        MutableStateFlow<ResponseStatus<List<Exercise>>>(ResponseStatus.Success(listOf()))
    val exercises get() = _exercises.asStateFlow()


    fun getExercises() {
        viewModelScope.launch(Dispatchers.IO) {
            _exercises.update { ResponseStatus.Loading() }
            _exercises.update { getExercisesUseCase() }
            Log.d(
                "ducna",
                "getExercises: ${_exercises.value} on thread ${Thread.currentThread().name}"
            )
        }
    }

}