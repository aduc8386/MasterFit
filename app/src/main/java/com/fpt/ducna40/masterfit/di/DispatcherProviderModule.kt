package com.fpt.ducna40.masterfit.di

import com.fpt.ducna40.masterfit.util.dispatcherprovider.DefaultDispatcherProvider
import com.fpt.ducna40.masterfit.util.dispatcherprovider.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DispatcherProviderModule {

    @Provides
    @Singleton
    fun providesDispatcherProvider(): DispatcherProvider = DefaultDispatcherProvider()

}