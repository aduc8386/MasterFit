package com.fpt.ducna40.masterfit.presentation

import android.Manifest
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.domain.usecase.UpdateDailyLoggingUseCase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AndroidEntryPoint
class LoggingReceiver : BroadcastReceiver() {

    @Inject
    lateinit var updateDailyLoggingUseCase: UpdateDailyLoggingUseCase

    override fun onReceive(context: Context, intent: Intent?) {
        CoroutineScope(Dispatchers.IO).launch {
            val workoutLogging = withContext(Dispatchers.Default) { updateDailyLoggingUseCase() }

            workoutLogging?.let {
                val pendingIntent = Intent(context, MainActivity::class.java).let {
                    PendingIntent.getActivity(
                        context,
                        0,
                        it,
                        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                    )
                }

                val remoteView = RemoteViews(context.packageName, R.layout.notification_daily).apply {
                    setTextViewText(
                        R.id.tv_notification_daily_steps,
                        "Steps: ${workoutLogging.currentStep}"
                    )
                    setTextViewText(
                        R.id.tv_notification_daily_calories,
                        "Calories: ${workoutLogging.currentCalories}"
                    )
                }

                val notificationBuilder = NotificationCompat.Builder(context, "master_fit").apply {
                    setSmallIcon(R.drawable.ic_app_logo)
                    setCustomBigContentView(remoteView)
                    setContentIntent(pendingIntent)
                    setDefaults(NotificationCompat.DEFAULT_ALL)
                    priority = NotificationCompat.PRIORITY_HIGH
                }


                NotificationManagerCompat.from(context).run {
                    if (ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.POST_NOTIFICATIONS
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        Log.d("ducna", "onReceive: permission denied")
                    }
                    notify(1, notificationBuilder.build())
                }
            }
        }
    }


}