package com.fpt.ducna40.masterfit.util.permissionhelper

import android.app.Activity
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class PermissionHelper constructor(
    private val activity: Activity,
    private val activityResultLauncher: ActivityResultLauncher<String>
) {

    fun requestPermission(setOfPermission: Set<String>) {
        for (permission in setOfPermission) {
            when {
                ContextCompat.checkSelfPermission(
                    activity,
                    permission
                ) == PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(activity, "Permission granted", Toast.LENGTH_SHORT).show()
                }

                activity.shouldShowRequestPermissionRationale(permission) -> {
                    activityResultLauncher.launch(permission)
                }

                else -> {
                    activityResultLauncher.launch(permission)
                }
            }
        }
    }

}