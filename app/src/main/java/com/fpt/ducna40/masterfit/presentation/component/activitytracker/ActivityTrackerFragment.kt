package com.fpt.ducna40.masterfit.presentation.component.activitytracker

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.databinding.FragmentActivityTrackerBinding
import com.fpt.ducna40.masterfit.extension.safeNavigate
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ActivityTrackerFragment : BaseFragment<FragmentActivityTrackerBinding>() {

    private val userViewModel: UserViewModel by activityViewModels()
    private val activityTrackerViewModel: ActivityTrackerViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_activity_tracker

    override fun onClick(v: View?) {

    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                userViewModel.currentUser.combine(activityTrackerViewModel.currentSteps) { user, currentSteps ->
                    handleState(user, currentSteps)
                }.collect()
            }
        }
    }

    private fun handleState(userResponseStatus: ResponseStatus<User>?, currentSteps: Float) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                (requireActivity() as MainActivity).hideLoading()
                Log.d("ducna", "handleState: called $userResponseStatus $currentSteps")
                if (userResponseStatus.data!!.goal == null) {
                    findNavController().safeNavigate(ActivityTrackerFragmentDirections.actionActivityTrackerFragmentToSetGoalFragment())
                    return
                }

                with(binding) {

                    val targetSteps = userResponseStatus.data!!.goal!!.stepsPerDay
                    val targetCalories = userResponseStatus.data!!.goal!!.caloriesBurnPerDay

                    gtvActivityTrackerStepsTarget.text = targetSteps.toString()
                    gtvActivityTrackerCaloriesTarget.text = targetCalories.toString()
                    wpvActivityTrackerStep.run {
                        setCurrent(currentSteps)
                        setTarget(targetSteps.toFloat())
                        setUnit("Steps")
                    }
                    gtvActivityTrackerSteps.text = currentSteps.toInt().toString()
                    wpvActivityTrackerCalories.run {
                        setCurrent(currentSteps * 0.04f)
                        setTarget(targetCalories.toFloat())
                        setUnit("Calories")
                    }
                    gtvActivityTrackerCalories.text = (currentSteps * 0.04).toInt().toString()
                    userResponseStatus.data!!.loggings?.let {
                        acvActivityTrackerDailySteps.setData(
                            it.takeLast(7),
                            userResponseStatus.data!!.goal!!
                        )
                    }
                }
            }

            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }

            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }

            else -> {

            }
        }
    }

    override fun bindView() {
        (requireActivity() as MainActivity).showBottomNavigation()
        userViewModel.getCurrentUser()
        activityTrackerViewModel.getCurrentStep()
    }
}