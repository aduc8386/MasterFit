package com.fpt.ducna40.masterfit.presentation.component.login

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.databinding.FragmentLoginBinding
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    private val userViewModel: UserViewModel by activityViewModels()

    override fun getLayoutId(): Int = R.layout.fragment_login

    override fun bindView() {

        (requireActivity() as MainActivity).hideBottomNavigation()

        with(binding) {
            tvLoginRegister.setOnClickListener(this@LoginFragment)
            btnLoginLogin.setOnClickListener(this@LoginFragment)
        }
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                userViewModel.currentUser.collectLatest {
                    handleUserState(it)
                }
            }
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                tvLoginRegister -> {
                    findNavController().navigate(LoginFragmentDirections.actionGlobalRegisterFragment())
                }

                btnLoginLogin -> {
                    userViewModel.login(
                        edtLoginEmail.text.toString(),
                        edtLoginPassword.text.toString()
                    )
                }
            }
        }
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<User>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                if (sharedPreferenceHelper.getIsLoggedIn()) {
                    (requireActivity() as MainActivity).hideLoading()
                    findNavController().navigate(
                        LoginFragmentDirections.actionLoginFragmentToLoginCompleteFragment()
                    )
                }
            }

            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }

            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }

            else -> {
            }
        }
    }

}