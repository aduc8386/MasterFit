package com.fpt.ducna40.masterfit.presentation.component.home

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.Exercise
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.databinding.FragmentHomeBinding
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.presentation.MainViewModel
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    private val userViewModel: UserViewModel by activityViewModels()
    private val mainViewModel: MainViewModel by activityViewModels()
    private val homeViewModel: HomeViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun bindView() {
        checkConditions()
        (requireActivity() as MainActivity).showBottomNavigation()
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            launch {
                userViewModel.currentUser.collectLatest {
                    Log.d("ducna", "observeViewModels home: $it")
                    handleUserState(it)
                }
            }
            launch {
                homeViewModel.exercises.collectLatest {
                    handleExercises(it)
                }
            }
        }
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

    private fun checkConditions() {
        if (mainViewModel.isFirstLaunch) {
            findNavController().navigate(R.id.splashFragment)
            return
        }
        if (sharedPreferenceHelper.getIsOpenFirstTime()) {
            findNavController().navigate(R.id.action_global_getStartedFragment)
            return
        }
        if (!sharedPreferenceHelper.getIsLoggedIn() && !sharedPreferenceHelper.getIsRemember()) {
            findNavController().navigate(R.id.action_global_loginFragment)
            return
        } else {
            userViewModel.login(
                sharedPreferenceHelper.getUserEmail()!!,
                sharedPreferenceHelper.getUserPassword()!!
            )
            homeViewModel.getExercises()
        }
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<User>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                (requireActivity() as MainActivity).hideLoading()
                with(binding) {
                    tvHomeUserFullName.text = userResponseStatus.data!!.fullName
                    bmiviewHomeBmiView.run {
                        setBmi(String.format("%.1f", userResponseStatus.data!!.getBmi()))
                        setBmiDesc(userResponseStatus.data!!.getBmiDesc())
                    }
                }
            }

            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }

            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }

            else -> {
                Log.d("ducna", "handleCurrentUser: called")
            }
        }
    }

    private fun handleExercises(listResponseStatus: ResponseStatus<List<Exercise>>) {
        when (listResponseStatus) {
            is ResponseStatus.Success -> {
                (requireActivity() as MainActivity).hideLoading()
                with(binding) {
                    rcvHomeExercise.run {
                        layoutManager =
                            LinearLayoutManager(
                                requireContext(),
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                        adapter = ExerciseAdapter(listResponseStatus.data!!)
                    }
                }
            }

            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), listResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }

            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }
        }
    }
}