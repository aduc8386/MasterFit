package com.fpt.ducna40.masterfit.presentation.component.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.model.Exercise
import com.fpt.ducna40.masterfit.databinding.ItemExerciseBinding

class ExerciseAdapter(private var exercises: List<Exercise>) :
    RecyclerView.Adapter<ExerciseAdapter.ExerciseHolder>() {

    class ExerciseHolder(val binding: ItemExerciseBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(exercise: Exercise) {
            with(binding) {
                val detail = "${exercise.exercises} Exercises | ${exercise.duration} Mins"

                val artDrawable = when (exercise.type) {
                    "full body" -> AppCompatResources.getDrawable(
                        root.context,
                        R.drawable.ic_full_body
                    )
                    "lower body" -> AppCompatResources.getDrawable(
                        root.context,
                        R.drawable.ic_lower_body
                    )
                    else -> AppCompatResources.getDrawable(
                        root.context,
                        R.drawable.ic_abs
                    )
                }

                tvWorkoutExerciseName.text = exercise.name
                tvWorkoutExerciseDetail.text = detail
                ivWorkoutExerciseArt.setImageDrawable(artDrawable)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseHolder {
        val binding = DataBindingUtil.inflate<ItemExerciseBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_exercise,
            parent,
            false
        )
        return ExerciseHolder(binding)
    }

    override fun getItemCount(): Int = exercises.size

    override fun onBindViewHolder(holder: ExerciseHolder, position: Int) {
        val exercise = exercises[position]
        holder.bind(exercise)
    }
}