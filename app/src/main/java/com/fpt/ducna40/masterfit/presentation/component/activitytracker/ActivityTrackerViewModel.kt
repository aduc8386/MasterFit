package com.fpt.ducna40.masterfit.presentation.component.activitytracker

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fpt.ducna40.masterfit.domain.usecase.GetCurrentStepsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ActivityTrackerViewModel @Inject constructor(
    private val getCurrentStepsUseCase: GetCurrentStepsUseCase
) : ViewModel() {

    private var _currentSteps =
        MutableStateFlow(0f)
    val currentSteps get() = _currentSteps.asStateFlow()

    fun getCurrentStep() {
        viewModelScope.launch(Dispatchers.IO) {
            getCurrentStepsUseCase().collect {
                _currentSteps.value = it
            }
        }
    }

}