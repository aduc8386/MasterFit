package com.fpt.ducna40.masterfit.data.repo

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.remote.api.ExerciseDatasource
import com.fpt.ducna40.masterfit.data.model.Exercise
import com.fpt.ducna40.masterfit.domain.repo.ExerciseRepository
import javax.inject.Inject

class ExerciseMockApiRepository @Inject constructor(
    private val exerciseDatasource: ExerciseDatasource
) : ExerciseRepository {
    override suspend fun getExercises(): ResponseStatus<List<Exercise>> = exerciseDatasource.getExercises()
}