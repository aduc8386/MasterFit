package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Exercise
import com.fpt.ducna40.masterfit.domain.repo.ExerciseRepository
import javax.inject.Inject

class GetExercisesUseCase @Inject constructor(
    private val exerciseRepository: ExerciseRepository
) {
    suspend operator fun invoke(): ResponseStatus<List<Exercise>> {
        return exerciseRepository.getExercises()
    }
}