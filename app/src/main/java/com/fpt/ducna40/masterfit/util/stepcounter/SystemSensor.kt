package com.fpt.ducna40.masterfit.util.stepcounter

import android.hardware.SensorEventListener

abstract class SystemSensor : SensorEventListener {
    protected var onValueChanged: OnStepChanged? = null

    abstract val isSensorExist: Boolean

    abstract fun startListening()
    abstract fun stopListening()

    fun setListener(listener: OnStepChanged) {
        onValueChanged = listener
    }
}

interface OnStepChanged {
    fun onStepChanged(value: List<Float>)
}