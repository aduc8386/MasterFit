package com.fpt.ducna40.masterfit.presentation.component.onboarding

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.FragmentOnboardingThirdBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment

class OnboardingThirdFragment : BaseFragment<FragmentOnboardingThirdBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_onboarding_third

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewPager2 = requireActivity().findViewById<ViewPager2>(R.id.vp_view_pager)

        binding.ivOnboardingThirdNext.setOnClickListener {
            viewPager2.currentItem = 3
        }

    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}