package com.fpt.ducna40.masterfit.data.datasource.remote.firebase

import android.util.Log
import com.fpt.ducna40.masterfit.constant.FirebaseReference
import com.fpt.ducna40.masterfit.data.RegisterRequest
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging
import com.fpt.ducna40.masterfit.di.UserDatabaseReferences
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class UserFirebaseDatasource @Inject constructor(
    @UserDatabaseReferences private val userFirebaseReference: DatabaseReference,
    private val firebaseAuth: FirebaseAuth,
) : UserDatasource {

    override suspend fun insertUser(registerRequest: RegisterRequest): ResponseStatus<User> {
        return try {
            val user = User(
                id = firebaseAuth.currentUser!!.uid,
                fullName = registerRequest.fullName,
                phoneNumber = registerRequest.phoneNumber,
                email = registerRequest.email,
                password = registerRequest.password,
                gender = registerRequest.gender,
                dateOfBirth = registerRequest.dateOfBirth,
                weight = registerRequest.weight,
                height = registerRequest.height,
            )

            userFirebaseReference.child(user.id).setValue(user).await()
            firebaseAuth.signOut()

            ResponseStatus.Success(user)


        } catch (exception: FirebaseException) {
            ResponseStatus.Error(exception.message!!)
        }
    }

    override suspend fun insertUserEmailNamePhonePassword(
        registerRequest: RegisterRequest
    ): ResponseStatus<User> {
        return try {
            firebaseAuth.createUserWithEmailAndPassword(
                registerRequest.email,
                registerRequest.password
            ).await()

            if (firebaseAuth.currentUser != null) {
                val registeredUser =
                    User(
                        id = firebaseAuth.currentUser!!.uid,
                        email = registerRequest.email,
                        fullName = registerRequest.fullName,
                        phoneNumber = registerRequest.phoneNumber,
                        password = registerRequest.password
                    )
                userFirebaseReference.child(registeredUser.id).setValue(registeredUser).await()
                ResponseStatus.Success(registeredUser)
            } else {
                ResponseStatus.Error("Something when wrong")
            }
        } catch (exception: FirebaseException) {
            ResponseStatus.Error(exception.message!!)
        }
    }

    override suspend fun getUserByEmailAndPassword(
        email: String,
        password: String
    ): ResponseStatus<User> {
        return try {
            firebaseAuth.signInWithEmailAndPassword(email, password).await()

            val user = userFirebaseReference.child(firebaseAuth.currentUser!!.uid).get()
                .await().getValue(User::class.java)

            Log.d("ducna", "getUserByEmailAndPassword in line 105: $user")
            if (user != null) ResponseStatus.Success(user) else ResponseStatus.Error("Null user")

        } catch (exception: FirebaseException) {
            ResponseStatus.Error(exception.message!!)
        }
    }

    override fun getCurrentUser(): Flow<ResponseStatus<User>> =
        getUser().combine(getWorkoutLoggings()) { userResponseStatus, workoutLoggings ->
            userResponseStatus.apply {
                data!!.loggings = workoutLoggings
            }
        }

    override fun getUser(): Flow<ResponseStatus<User>> = callbackFlow {

        if (firebaseAuth.currentUser == null) {
            trySend(ResponseStatus.Error("Null user"))
        }

        val currentUserReference = firebaseAuth.currentUser?.uid?.let {
            userFirebaseReference.child(
                it
            )
        }

        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val userLogin = dataSnapshot.getValue(User::class.java)

                trySend(
                    if (userLogin != null)
                        ResponseStatus.Success(userLogin)
                    else ResponseStatus.Error("Not found user")
                )
            }
            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        currentUserReference?.addValueEventListener(listener)

        awaitClose {
            currentUserReference?.removeEventListener(listener)
        }
    }

    override suspend fun setGoal(goal: Goal): ResponseStatus<User> {

        if (firebaseAuth.currentUser == null) {
            return ResponseStatus.Error("User null")
        }

        val currentUserGoalReference = firebaseAuth.currentUser?.uid?.let { userId ->
            userFirebaseReference.child(
                "$userId/${FirebaseReference.GOAL}"
            )
        }

        currentUserGoalReference?.setValue(goal)?.await()

        return ResponseStatus.Success(
            userFirebaseReference.child(firebaseAuth.currentUser!!.uid).get().await()
                .getValue(User::class.java)!!
        )
    }

    override fun getWorkoutLoggings(): Flow<List<WorkoutLogging>> {

        val currentUserWorkoutLoggingsReference = firebaseAuth.currentUser?.uid?.let { userId ->
            userFirebaseReference.child(
                "$userId/${FirebaseReference.WORKOUT_LOGGINGS}"
            )
        }

        return callbackFlow {
            val listener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val workoutLoggings = mutableListOf<WorkoutLogging>()

                    for (workLogging in snapshot.children) {
                        workLogging.getValue(WorkoutLogging::class.java)?.let {
                            workoutLoggings.add(it)
                        }
                    }

                    trySend(workoutLoggings)
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("ducna", "onCancelled: called")
                }
            }


            currentUserWorkoutLoggingsReference?.addValueEventListener(listener)
            awaitClose { currentUserWorkoutLoggingsReference?.removeEventListener(listener) }

        }
    }

    override suspend fun updateLogging(workoutLogging: WorkoutLogging) {
        firebaseAuth.currentUser?.let {
            userFirebaseReference.child(it.uid)
                .child(FirebaseReference.WORKOUT_LOGGINGS).child(workoutLogging.id)
                .setValue(workoutLogging).await()
        }
    }


}