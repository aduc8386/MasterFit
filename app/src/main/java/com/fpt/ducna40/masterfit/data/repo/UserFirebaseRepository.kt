package com.fpt.ducna40.masterfit.data.repo

import com.fpt.ducna40.masterfit.data.RegisterRequest
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.remote.firebase.UserDatasource
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UserFirebaseRepository @Inject constructor(
    private val userDatasource: UserDatasource
) : UserRepository {
    override suspend fun insertUserFullInformation(registerRequest: RegisterRequest): ResponseStatus<User> {
        return userDatasource.insertUser(registerRequest)
    }

    override suspend fun insertUserEmailNamePhonePassword(registerRequest: RegisterRequest): ResponseStatus<User> {
        return userDatasource.insertUserEmailNamePhonePassword(registerRequest)
    }

    override suspend fun getUserByEmailAndPassword(
        email: String,
        password: String
    ): ResponseStatus<User> {
        return userDatasource.getUserByEmailAndPassword(email, password)
    }

    override fun getCurrentUser(): Flow<ResponseStatus<User>> = userDatasource.getCurrentUser()

    override suspend fun setGoal(goal: Goal): ResponseStatus<User> = userDatasource.setGoal(goal)

    override suspend fun getWorkoutLoggings(): Flow<List<WorkoutLogging>> =
        userDatasource.getWorkoutLoggings()

    override suspend fun updateLogging(workoutLogging: WorkoutLogging) {
        userDatasource.updateLogging(workoutLogging)
    }

}