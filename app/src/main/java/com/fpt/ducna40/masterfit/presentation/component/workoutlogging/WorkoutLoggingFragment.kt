package com.fpt.ducna40.masterfit.presentation.component.workoutlogging

import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging
import com.fpt.ducna40.masterfit.databinding.FragmentWorkoutLoggingBinding
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WorkoutLoggingFragment : BaseFragment<FragmentWorkoutLoggingBinding>() {

    private val userViewModel: UserViewModel by activityViewModels()

    override fun getLayoutId(): Int = R.layout.fragment_workout_logging

    override fun observeViewModels() {
        lifecycleScope.launch {
            userViewModel.currentUser.collectLatest {
                handleUserState(it)
            }
        }
    }

    override fun bindView() {
        userViewModel.getCurrentUser()
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<User>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                if (userResponseStatus.data!!.loggings != null && userResponseStatus.data!!.goal != null) {
                    with(binding) {
                        rcvWorkoutLogging.run {
                            layoutManager =
                                LinearLayoutManager(
                                    requireContext(),
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                            adapter = WorkoutLoggingAdapter(
                                userResponseStatus.data!!.loggings!!.takeLast(7),
                                userResponseStatus.data!!.goal!!
                            )
                        }
                    }
                }
            }

            else -> {}
        }
    }
}