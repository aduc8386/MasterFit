package com.fpt.ducna40.masterfit.presentation.component.onboarding

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.FragmentOnboardingSecondBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment

class OnboardingSecondFragment : BaseFragment<FragmentOnboardingSecondBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_onboarding_second

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewPager2 = requireActivity().findViewById<ViewPager2>(R.id.vp_view_pager)

        binding.ivOnboardingSecondNext.setOnClickListener {
            viewPager2.currentItem = 2
        }

    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}