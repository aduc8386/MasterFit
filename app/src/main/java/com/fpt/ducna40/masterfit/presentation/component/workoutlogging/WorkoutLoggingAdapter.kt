package com.fpt.ducna40.masterfit.presentation.component.workoutlogging

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging
import com.fpt.ducna40.masterfit.databinding.ItemExerciseBinding
import com.fpt.ducna40.masterfit.databinding.ItemWorkoutLoggingBinding
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class WorkoutLoggingAdapter(
    private val workoutLoggings: List<WorkoutLogging>,
    private val goal: Goal
) :
    RecyclerView.Adapter<WorkoutLoggingAdapter.WorkoutLoggingHolder>() {

    inner class WorkoutLoggingHolder(private val binding: ItemWorkoutLoggingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(workoutLogging: WorkoutLogging) {
            with(binding) {
                tvItemWorkoutLoggingDate.text =
                    SimpleDateFormat("EEE, MMM d yyyy", Locale.US).format(
                        Date(workoutLogging.id.toLong())
                    )

                wpvItemWorkoutLoggingSteps.run {
                    setTarget(goal.stepsPerDay.toFloat())
                    setCurrent(workoutLogging.currentStep.toFloat())
                    setUnit("Steps")
                }

                wpvItemWorkoutLoggingCalories.run {
                    setTarget(goal.caloriesBurnPerDay.toFloat())
                    setCurrent(workoutLogging.currentCalories.toFloat())
                    setUnit("Calories")
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkoutLoggingHolder {
        val binding = DataBindingUtil.inflate<ItemWorkoutLoggingBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_workout_logging,
            parent,
            false
        )

        return WorkoutLoggingHolder(binding)
    }

    override fun getItemCount(): Int = workoutLoggings.size

    override fun onBindViewHolder(holder: WorkoutLoggingHolder, position: Int) {
        val workoutLogging = workoutLoggings[position]
        holder.bind(workoutLogging)
    }


}