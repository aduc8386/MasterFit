package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import javax.inject.Inject

class SetGoalUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend operator fun invoke(
        title: String,
        targetDate: String,
        targetWeight: Double,
        stepsPerDay: Int,
        caloriesBurnedPerDay: Double
    ): ResponseStatus<User> {

        return userRepository.setGoal(
            Goal(
                title,
                stepsPerDay,
                targetWeight,
                caloriesBurnedPerDay,
                targetDate
            )
        )
    }

}