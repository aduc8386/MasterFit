package com.fpt.ducna40.masterfit.presentation.customview

import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.fpt.ducna40.masterfit.R

class GradientTextView(context: Context, attrs: AttributeSet?) : AppCompatTextView(context, attrs) {

    private var startColor = Color.BLUE
    private var endColor = Color.RED

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.GradientTextView,
            0,
            0
        ).apply {
            try {
                startColor = getColor(R.styleable.GradientTextView_startColor, Color.BLUE)
                endColor = getColor(R.styleable.GradientTextView_endColor, Color.RED)
            } finally {
                recycle()
            }
        }

        paint.shader = LinearGradient(
            0f, paint.fontSpacing / 2, paint.measureText(text.toString()), paint.fontSpacing / 2,
            startColor,
            endColor,
            Shader.TileMode.CLAMP
        )

    }
}