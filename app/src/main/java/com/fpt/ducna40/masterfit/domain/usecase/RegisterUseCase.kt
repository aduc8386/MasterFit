package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.RegisterRequest
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(
        fullName: String,
        phoneNumber: String,
        email: String,
        password: String,
        gender: String,
        dateOfBirth: String,
        height: Double,
        weight: Double
    ): ResponseStatus<User> {
        when {
            dateOfBirth.isEmpty() -> return ResponseStatus.Error("Date of birth can't be empty")
            weight.isNaN() -> return ResponseStatus.Error("Weight must be a number")
            height.isNaN() -> return ResponseStatus.Error("Height must be a number")
            else -> {

                var notEmptyFullName = fullName
                var notEmptyEmail = email
                var notEmptyPhoneNumber = phoneNumber
                var notEmptyPassword = password

                if (notEmptyFullName.isEmpty()) {
                    notEmptyFullName = sharedPreferenceHelper.getUserFullName()!!
                }
                if (notEmptyEmail.isEmpty()) {
                    notEmptyEmail = sharedPreferenceHelper.getUserEmail()!!
                }
                if (notEmptyPassword.isEmpty()) {
                    notEmptyPassword = sharedPreferenceHelper.getUserPassword()!!
                }
                if (notEmptyPhoneNumber.isEmpty()) {
                    notEmptyPhoneNumber = sharedPreferenceHelper.getUserPhoneNumber()!!
                }

                val responseStatus = userRepository.insertUserFullInformation(
                    RegisterRequest(
                        fullName = notEmptyFullName,
                        phoneNumber = notEmptyPhoneNumber,
                        email = notEmptyEmail,
                        password = notEmptyPassword,
                        gender = gender,
                        dateOfBirth = dateOfBirth,
                        height = height,
                        weight = weight
                    )
                )
                if (responseStatus is ResponseStatus.Success) {
                    sharedPreferenceHelper.setIsCompleteProfile(true)
                }
                return responseStatus
            }
        }
    }

}