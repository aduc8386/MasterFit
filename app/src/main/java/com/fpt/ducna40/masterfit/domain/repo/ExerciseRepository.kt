package com.fpt.ducna40.masterfit.domain.repo

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Exercise

interface ExerciseRepository {

    suspend fun getExercises(): ResponseStatus<List<Exercise>>

}