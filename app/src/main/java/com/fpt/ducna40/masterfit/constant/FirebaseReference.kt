package com.fpt.ducna40.masterfit.constant

object FirebaseReference {
    const val USER = "users"
    const val GOAL = "goal"
    const val WORKOUT_LOGGINGS = "loggings"
}