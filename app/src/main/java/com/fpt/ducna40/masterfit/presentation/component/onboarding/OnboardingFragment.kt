package com.fpt.ducna40.masterfit.presentation.component.onboarding

import android.os.Bundle
import android.view.View
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.FragmentOnboardingBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFragment : BaseFragment<FragmentOnboardingBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_onboarding

    override fun bindView() {
        OnboardingAdapter(
            listOf(
                OnboardingFirstFragment(),
                OnboardingSecondFragment(),
                OnboardingThirdFragment(),
                OnboardingFourthFragment(),
            ),
            requireActivity().supportFragmentManager,
            lifecycle
        ).run {
            binding.vpViewPager.adapter = this
        }
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

}