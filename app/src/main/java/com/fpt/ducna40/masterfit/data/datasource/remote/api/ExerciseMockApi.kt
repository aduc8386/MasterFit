package com.fpt.ducna40.masterfit.data.datasource.remote.api

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Exercise
import retrofit2.http.GET

interface ExerciseMockApi {

    @GET("exercise")
    suspend fun getExercises(): List<Exercise>
}