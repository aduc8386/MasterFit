package com.fpt.ducna40.masterfit.di

import com.fpt.ducna40.masterfit.util.imageloader.GlideImageLoader
import com.fpt.ducna40.masterfit.util.imageloader.ImageLoader
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class ImageLoaderModule {

    @Binds
    abstract fun bindsGlideImageLoader(glideImageLoader: GlideImageLoader): ImageLoader

}