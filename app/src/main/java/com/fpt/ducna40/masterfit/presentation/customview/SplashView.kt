package com.fpt.ducna40.masterfit.presentation.customview

import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.fpt.ducna40.masterfit.R

class SplashView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private val paintIcon: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var height = 86
    private var width = 186
    private var appLogoOpacityAnimator: ValueAnimator
    private var appLogoOpacity: Double = 1.0

    init {

        appLogoOpacityAnimator = AnimatorInflater.loadAnimator(
            context,
            R.animator.animator_app_logo_opacity
        ) as ValueAnimator

        appLogoOpacityAnimator.addUpdateListener {
            appLogoOpacity = (it.animatedValue as Float).toDouble()
            Log.d("ducna", "update value in line 34: ${it.animatedValue}")
            invalidate()
        }

        appLogoOpacityAnimator.start()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        setMeasuredDimension(
            width.toDp(),
            height.toDp()
        )

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawLogo(canvas)

    }

    private fun drawLogo(canvas: Canvas) {
        val logoBitmap =
            ResourcesCompat.getDrawable(resources, R.drawable.ic_app_logo, null)!!
                .toBitmap(
                    width.toDp(),
                    height.toDp()
                )

        paintIcon.alpha = (appLogoOpacity * 255).toInt()

        val rect = RectF(
            paddingStart.toFloat(),
            paddingTop.toFloat(),
            paddingStart.toFloat() + logoBitmap.width,
            paddingTop.toFloat() + logoBitmap.height
        )

        canvas.drawBitmap(logoBitmap, null, rect, paintIcon)
    }

    private fun Int.toDp(): Int {
        return (this * resources.displayMetrics.density + 0.5f).toInt()
    }

}