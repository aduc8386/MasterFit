package com.fpt.ducna40.masterfit.presentation.component.login

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.databinding.FragmentLoginCompleteBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment

class LoginCompleteFragment : BaseFragment<FragmentLoginCompleteBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_login_complete

    override fun bindView() {
        with(binding) {
            btnLoginCompleteGoToHome.setOnClickListener(this@LoginCompleteFragment)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                btnLoginCompleteGoToHome -> {
                    findNavController().navigate(LoginCompleteFragmentDirections.actionLoginCompleteFragmentToNavMain())
                }
                else -> {}
            }
        }
    }
}