package com.fpt.ducna40.masterfit.presentation.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<VB : ViewBinding>(
) : Fragment(), OnClickListener {

    private var _binding: VB? = null
    val binding get() = _binding!!

    protected abstract fun getLayoutId(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d("ducnalifcycle", "onAttach: ${this.javaClass.simpleName} called")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        observeViewModels()
        Log.d("ducnalifcycle", "onCreateView: ${this.javaClass.simpleName} called")
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("ducnalifcycle", "onViewCreated: ${this.javaClass.simpleName} called")
        bindView()
    }

    open fun observeViewModels() {}

    open fun bindView() {}

    override fun onClick(v: View?) {}

    override fun onDestroy() {
        super.onDestroy()
        Log.d("ducnalifcycle", "onDestroy: ${this.javaClass.simpleName} called")
        _binding = null
    }

    override fun onDetach() {
        Log.d("ducnalifcycle", "onDetach: ${this.javaClass.simpleName} called")
        super.onDetach()
    }

}