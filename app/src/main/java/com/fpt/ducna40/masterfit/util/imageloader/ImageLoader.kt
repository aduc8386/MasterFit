package com.fpt.ducna40.masterfit.util.imageloader

import android.widget.ImageView

interface ImageLoader {
    fun load(imageView: ImageView, url: String)
}