package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCurrentUserUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    operator fun invoke(): Flow<ResponseStatus<User>> {
        return userRepository.getCurrentUser()
    }

}