package com.fpt.ducna40.masterfit.data

data class RegisterRequest(
    val fullName: String,
    val phoneNumber: String,
    val email: String,
    val password: String,
    val gender: String? = null,
    val dateOfBirth: String? = null,
    val height: Double = 0.0,
    val weight: Double = 0.0
)
