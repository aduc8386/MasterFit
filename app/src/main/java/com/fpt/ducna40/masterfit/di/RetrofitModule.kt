package com.fpt.ducna40.masterfit.di

import com.fpt.ducna40.masterfit.data.datasource.remote.api.ExerciseMockApiDatasource
import com.fpt.ducna40.masterfit.data.datasource.remote.api.ExerciseDatasource
import com.fpt.ducna40.masterfit.data.datasource.remote.api.ExerciseMockApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {


    @Provides
    @Singleton
    fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    @Singleton
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit
        .Builder()
        .baseUrl("https://d541b64c-f12c-4efe-8630-921e6e348764.mock.pstmn.io/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()


    @Provides
    @Singleton
    fun providesExerciseMockApi(retrofit: Retrofit): ExerciseMockApi =
        retrofit.create(ExerciseMockApi::class.java)

    @Provides
    @Singleton
    fun providesExerciseMockApiDatasource(
        exerciseMockApi: ExerciseMockApi,
    ): ExerciseDatasource = ExerciseMockApiDatasource(exerciseMockApi)

}