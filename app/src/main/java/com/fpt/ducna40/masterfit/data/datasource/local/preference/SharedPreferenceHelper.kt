package com.fpt.ducna40.masterfit.data.datasource.local.preference

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import javax.inject.Inject

class SharedPreferenceHelper @Inject constructor(private val sharedPreferences: SharedPreferences) {

    fun setUserEmail(email: String) {
        sharedPreferences.edit {
            putString(USER_EMAIL, email)
            apply()
        }
        Log.d("ducna", "setUserEmail: set $email")
    }

    fun getUserEmail() = sharedPreferences.getString(USER_EMAIL, "")

    fun setUserPassword(password: String) {
        sharedPreferences.edit {
            putString(USER_PASSWORD, password)
            apply()
        }
        Log.d("ducna", "setUserPassword: set $password")
    }

    fun getUserPassword() = sharedPreferences.getString(USER_PASSWORD, "")

    fun setUserFullName(fullName: String) {
        sharedPreferences.edit {
            putString(USER_FULL_NAME, fullName)
            apply()
        }
        Log.d("ducna", "setUserFullName: set $fullName")
    }

    fun getUserFullName() = sharedPreferences.getString(USER_FULL_NAME, "")

    fun setUserPhoneNumber(phoneNumber: String) {
        sharedPreferences.edit {
            putString(USER_PHONE_NUMBER, phoneNumber)
            apply()
        }
        Log.d("ducna", "setUserPhoneNumber: set $phoneNumber")
    }

    fun getUserPhoneNumber() = sharedPreferences.getString(USER_PHONE_NUMBER, "")

    fun setIsRemember(isRemember: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_REMEMBER, isRemember)
            apply()
        }
        Log.d("ducna", "setIsRemember: set $isRemember")
    }

    fun getIsRemember() = sharedPreferences.getBoolean(IS_REMEMBER, false)

    fun setIsOpenFirstTime(isOpenFirstTime: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_OPEN_FIRST_TIME, isOpenFirstTime)
            apply()
        }
        Log.d("ducna", "setIsOpenFirstTime: set $isOpenFirstTime")
    }

    fun getIsOpenFirstTime() = sharedPreferences.getBoolean(IS_OPEN_FIRST_TIME, true)

    fun setIsLoggedIn(isLoggedIn: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_LOGGED_IN, isLoggedIn)
            apply()
        }
        Log.d("ducna", "setIsLoggedIn: set $isLoggedIn")
    }

    fun getIsLoggedIn() = sharedPreferences.getBoolean(IS_LOGGED_IN, false)

    fun setIsCompleteProfile(isCompleteProfile: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_COMPLETE_PROFILE, isCompleteProfile)
            apply()
        }
        Log.d("ducna", "setIsCompleteProfile: set $isCompleteProfile")
    }

    fun getIsCompleteProfile() = sharedPreferences.getBoolean(IS_COMPLETE_PROFILE, false)

    fun getStepsCounted() = sharedPreferences.getInt(STEPS_COUNTED, 0)

    fun setStepsCounted(steps: Int) {
        sharedPreferences.edit {
            putInt(STEPS_COUNTED, steps)
            apply()
        }
        Log.d("ducna", "setStepsCounted: set $steps")
    }

    fun getCurrentSteps() = sharedPreferences.getInt(CURRENT_STEP, 0)

    fun setCurrentSteps(steps: Int) {
        sharedPreferences.edit {
            putInt(CURRENT_STEP, steps)
            apply()
        }
        Log.d("ducna", "setCurrentSteps: set $steps")
    }

    companion object {
        const val USER_EMAIL: String = "USER_EMAIL"
        const val USER_PASSWORD: String = "USER_PASSWORD"
        const val USER_FULL_NAME: String = "USER_FULL_NAME"
        const val USER_PHONE_NUMBER: String = "USER_PHONE_NUMBER"
        const val IS_OPEN_FIRST_TIME: String = "IS_OPEN_FIRST_TIME"
        const val IS_LOGGED_IN: String = "IS_LOGGED_IN"
        const val IS_REMEMBER: String = "IS_REMEMBER"
        const val IS_COMPLETE_PROFILE: String = "IS_COMPLETE_PROFILE"
        const val STEPS_COUNTED: String = "STEPS_COUNTED"
        const val CURRENT_STEP: String = "CURRENT_STEP"
    }

}