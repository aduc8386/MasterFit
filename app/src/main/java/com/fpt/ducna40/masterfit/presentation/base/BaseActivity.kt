package com.fpt.ducna40.masterfit.presentation.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.viewbinding.ViewBinding
import com.fpt.ducna40.masterfit.R

abstract class BaseActivity<VB : ViewBinding>(
) : AppCompatActivity() {

    lateinit var binding: VB

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        bindView()
    }

    open fun bindView() {}

}