package com.fpt.ducna40.masterfit.presentation.component.register

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.databinding.FragmentRegisterBinding
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>() {

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    private val userViewModel: UserViewModel by activityViewModels()
    override fun getLayoutId(): Int = R.layout.fragment_register

    override fun observeViewModels() {
        Log.d("ducna", "observeViewModels in line 30: $userViewModel")
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                userViewModel.currentUser.collectLatest {
                    Log.d("ducna", "observeViewModels in line 32: ${it?.data}")
                    handleUserState(it)
                }
            }
        }
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<User>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                (requireActivity() as MainActivity).hideLoading()
                findNavController().navigate(
                    RegisterFragmentDirections.actionRegisterFragmentToRegisterCompleteProfileFragment()
                )
            }
            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }
            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }
            else -> {}
        }
    }

    override fun bindView() {

        if (sharedPreferenceHelper.getUserEmail()!!.isNotEmpty()) {
            findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToRegisterCompleteProfileFragment())
            return
        }

        with(binding) {
            btnRegisterRegister.setOnClickListener(this@RegisterFragment)
            tvRegisterLogin.setOnClickListener(this@RegisterFragment)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                btnRegisterRegister -> {
                    userViewModel.registerWithEmailNamePhonePassword(
                        edtRegisterEmail.text.toString(),
                        edtRegisterFullName.text.toString(),
                        edtRegisterPhoneNumber.text.toString(),
                        edtRegisterPassword.text.toString()
                    )
                }
                tvRegisterLogin -> findNavController().navigate(RegisterFragmentDirections.actionGlobalLoginFragment())
            }
        }
    }
}