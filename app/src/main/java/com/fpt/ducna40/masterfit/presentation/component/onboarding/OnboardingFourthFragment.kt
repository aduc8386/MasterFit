package com.fpt.ducna40.masterfit.presentation.component.onboarding

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.databinding.FragmentOnboardingFourthBinding
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnboardingFourthFragment : BaseFragment<FragmentOnboardingFourthBinding>() {

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    override fun getLayoutId(): Int = R.layout.fragment_onboarding_fourth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivOnboardingFourthNext.setOnClickListener {
            sharedPreferenceHelper.setIsOpenFirstTime(false)
            findNavController().navigate(OnboardingFragmentDirections.actionGlobalLoginFragment())
        }

    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}