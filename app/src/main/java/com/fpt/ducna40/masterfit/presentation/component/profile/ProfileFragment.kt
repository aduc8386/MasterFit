package com.fpt.ducna40.masterfit.presentation.component.profile

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.databinding.FragmentProfileBinding
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private val userViewModel: UserViewModel by activityViewModels()

    override fun getLayoutId(): Int = R.layout.fragment_profile

    override fun bindView() {
        with(binding) {

        }
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                userViewModel.currentUser.collectLatest {
                    handleUserState(it)
                }
            }
        }
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<User>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                (requireActivity() as MainActivity).hideLoading()
                bindUserProfile(userResponseStatus.data!!)
            }
            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }
            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }
            else -> {
            }
        }
    }

    private fun bindUserProfile(user: User) {
        with(binding) {
            val currentYear: Int = Calendar.getInstance().get(Calendar.YEAR)
            val yearOfBirth = user.dateOfBirth!!.substring(5).toInt()

            tvProfileName.text = user.fullName
            tvProfileHeight.text = user.height.toString()
            tvProfileWeight.text = user.weight.toString()
            tvProfileAge.text = "${currentYear - yearOfBirth}"
        }
    }
}