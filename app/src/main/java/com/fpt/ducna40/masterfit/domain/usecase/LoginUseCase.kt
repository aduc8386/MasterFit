package com.fpt.ducna40.masterfit.domain.usecase

import android.util.Log
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {
    suspend operator fun invoke(email: String, password: String): ResponseStatus<User> {

        when{
            email.isEmpty() -> return ResponseStatus.Error("Email can't be empty")
            password.isEmpty() -> return ResponseStatus.Error("Password can't be empty")
        }

        val responseStatus = userRepository.getUserByEmailAndPassword(email, password)

        if(responseStatus is ResponseStatus.Success) {
            sharedPreferenceHelper.setUserEmail(email)
            sharedPreferenceHelper.setUserPassword(password)
            sharedPreferenceHelper.setIsLoggedIn(true)
            sharedPreferenceHelper.setIsRemember(true)
        }

        return responseStatus
    }
}