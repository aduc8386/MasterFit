package com.fpt.ducna40.masterfit.presentation.component.register

import android.app.DatePickerDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.databinding.FragmentRegisterCompleteProfileBinding
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RegisterCompleteProfileFragment : BaseFragment<FragmentRegisterCompleteProfileBinding>() {

    private val userViewModel: UserViewModel by activityViewModels()

    override fun getLayoutId(): Int = R.layout.fragment_register_complete_profile

    override fun bindView() {
        with(binding) {
            btnRegisterCompleteNext.setOnClickListener(this@RegisterCompleteProfileFragment)
            edtRegisterCompleteDateOfBirth.setOnClickListener(this@RegisterCompleteProfileFragment)
        }
    }

    override fun observeViewModels() {
        Log.d("ducna", "observeViewModels in line 36: $userViewModel")
        lifecycleScope.launch {
            userViewModel.currentUser.collectLatest {
                Log.d("ducna", "observeViewModels in line 38: ${it?.data}")
                handleUserState(it)
            }
        }
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<User>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                if (userResponseStatus.data!!.isFullInformation()) {
                    (requireActivity() as MainActivity).hideLoading()
                    findNavController().navigate(
                        RegisterCompleteProfileFragmentDirections.actionGlobalLoginFragment()
                    )
                }
            }
            is ResponseStatus.Error -> {
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }
            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }
            else -> {}
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                btnRegisterCompleteNext -> {
                    val genders = requireActivity().resources.getStringArray(R.array.gender)

                    userViewModel.registerUser(
                        genders[spnRegisterCompleteGender.selectedItemPosition],
                        edtRegisterCompleteDateOfBirth.text.toString(),
                        edtRegisterCompleteHeight.text.toString().toDouble(),
                        edtRegisterCompleteWeight.text.toString().toDouble(),
                    )
                }
                edtRegisterCompleteDateOfBirth -> {
                    DatePickerDialog(requireActivity()).run {
                        setOnDateSetListener { _, year, month, dayOfMonth ->
                            val dateOfBirth = "$dayOfMonth/${month + 1}/$year"
                            edtRegisterCompleteDateOfBirth.setText(dateOfBirth)
                        }
                        show()
                    }
                }
            }
        }
    }
}