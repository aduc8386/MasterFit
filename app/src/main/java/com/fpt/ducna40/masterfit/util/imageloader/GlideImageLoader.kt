package com.fpt.ducna40.masterfit.util.imageloader

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.fpt.ducna40.masterfit.R
import javax.inject.Inject

class GlideImageLoader @Inject constructor() : ImageLoader {
    override fun load(imageView: ImageView, url: String) {
        Glide.with(imageView.context)
            .load(url)
            .centerCrop()
            .error(R.drawable.ic_app_logo)
            .into(imageView)
    }
}