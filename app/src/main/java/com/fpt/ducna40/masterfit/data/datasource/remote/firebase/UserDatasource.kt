package com.fpt.ducna40.masterfit.data.datasource.remote.firebase

import com.fpt.ducna40.masterfit.data.RegisterRequest
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Goal
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging
import kotlinx.coroutines.flow.Flow

interface UserDatasource {

    suspend fun insertUser(registerRequest: RegisterRequest): ResponseStatus<User>

    suspend fun insertUserEmailNamePhonePassword(
        registerRequest: RegisterRequest
    ): ResponseStatus<User>

    suspend fun getUserByEmailAndPassword(
        email: String,
        password: String
    ): ResponseStatus<User>

    fun getUser(): Flow<ResponseStatus<User>>

    fun getWorkoutLoggings(): Flow<List<WorkoutLogging>>

    fun getCurrentUser(): Flow<ResponseStatus<User>>

    suspend fun setGoal(goal: Goal): ResponseStatus<User>

    suspend fun updateLogging(workoutLogging: WorkoutLogging)

}