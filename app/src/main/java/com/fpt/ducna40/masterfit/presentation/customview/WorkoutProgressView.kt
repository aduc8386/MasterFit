package com.fpt.ducna40.masterfit.presentation.customview

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging

class WorkoutProgressView(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    private val innerCirclePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val outlineCircleBackgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val outlineCirclePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val lightBlue = ContextCompat.getColor(context, R.color.blue_light)
    private val darkBlue = ContextCompat.getColor(context, R.color.blue_dark)
    private val lightPurple = ContextCompat.getColor(context, R.color.purple_light)
    private val darkPurple = ContextCompat.getColor(context, R.color.purple_dark)
    private val black10 = ContextCompat.getColor(context, R.color.black_10)

    private var size: Int = 0
    private var radius: Int = 0

    private val progressLengthAnimator: Animator
    private var progressLength: Float = 0f

    private var current = 0f
    private var target = 0f
    private var unit = "unit"

    fun setCurrent(c: Float) {
        current = c
        invalidate()
    }

    fun setTarget(t: Float) {
        target = t
        invalidate()
    }

    fun setUnit(u: String) {
        unit = u
        invalidate()
    }

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.WorkoutProgressView,
            0,
            0
        ).apply {
            try {
                current = getFloat(R.styleable.WorkoutProgressView_current, 0f)
                target = getFloat(R.styleable.WorkoutProgressView_target, 0f)
                unit = getString(R.styleable.WorkoutProgressView_unit).toString()
            } finally {
                recycle()
            }
        }

        progressLengthAnimator = AnimatorInflater.loadAnimator(
            context,
            R.animator.animator_progress_length
        ) as ValueAnimator

        progressLengthAnimator.addUpdateListener {
            progressLength = (it.animatedValue as Float).toFloat()
            Log.d("ducna", "update value in line 34: ${it.animatedValue}")
            invalidate()
        }

        progressLengthAnimator.start()


    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        size = measuredWidth.coerceAtMost(measuredHeight)
        radius = size / 2


        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawInnerCircle(canvas)

        drawOutlineCircle(canvas)

    }

    private fun drawOutlineCircle(canvas: Canvas) {
        outlineCircleBackgroundPaint.apply {
            color = black10
            strokeWidth = radius.toFloat() * 0.17f
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
        }

        canvas.drawCircle(
            radius.toFloat(),
            radius.toFloat(),
            size / 2 * 0.9f,
            outlineCircleBackgroundPaint
        )

        val rectF = RectF(size / 2 * 0.1f, size / 2 * 0.1f, size * 0.95f, size * 0.95f)
        outlineCirclePaint.apply {
            strokeWidth = radius.toFloat() * 0.17f
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
            shader = LinearGradient(
                0f,
                size.toFloat(),
                size.toFloat(),
                0f,
                lightPurple,
                darkPurple,
                Shader.TileMode.CLAMP
            )
        }

        val angel = (current / target) * 360f

        canvas.drawArc(rectF, -180f, angel * progressLength, false, outlineCirclePaint)

    }

    private fun drawInnerCircle(canvas: Canvas) {
        val innerCircleRadius = radius * 0.75

        innerCirclePaint.apply {
            shader = LinearGradient(
                0f,
                (innerCircleRadius * 2).toFloat(),
                (innerCircleRadius * 2).toFloat(),
                0f,
                lightBlue,
                darkBlue,
                Shader.TileMode.CLAMP
            )
        }

        canvas.drawCircle(
            radius.toFloat(),
            radius.toFloat(),
            innerCircleRadius.toFloat(),
            innerCirclePaint
        )

        val left = if ((target - current) < 0) {
            0
        } else {
            target - current
        }

        drawText(
            canvas = canvas,
            text = "${left.toInt()} ${unit.lowercase()}",
            size = 8,
            align = Paint.Align.CENTER,
            x = radius.toFloat(),
            y = (radius.toFloat() * 2) * 0.45f
        )
        drawText(
            canvas = canvas,
            text = "left",
            size = 8,
            align = Paint.Align.CENTER,
            x = radius.toFloat(),
            y = (radius.toFloat() * 2) * 0.6f
        )

    }

    private fun drawText(
        canvas: Canvas,
        text: String,
        size: Int,
        align: Paint.Align = Paint.Align.LEFT,
        typeFace: Typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL),
        x: Float,
        y: Float
    ) {
        textPaint.apply {
            textAlign = align
            textSize = size * resources.displayMetrics.density
            color = Color.WHITE
            typeface = typeFace
        }
        canvas.drawText(
            text, x, y, textPaint
        )
    }
}