package com.fpt.ducna40.masterfit.data.datasource.remote.api

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Exercise
import javax.inject.Inject

interface ExerciseDatasource {

    suspend fun getExercises(): ResponseStatus<List<Exercise>>

}

