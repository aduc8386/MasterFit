package com.fpt.ducna40.masterfit.data.datasource.remote.api

import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.Exercise
import javax.inject.Inject

class ExerciseMockApiDatasource @Inject constructor(
    private val exerciseMockApi: ExerciseMockApi
) : ExerciseDatasource {
    override suspend fun getExercises(): ResponseStatus<List<Exercise>> =
        ResponseStatus.Success(exerciseMockApi.getExercises())

}