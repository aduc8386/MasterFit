package com.fpt.ducna40.masterfit.di

import android.content.Context
import com.fpt.ducna40.masterfit.util.stepcounter.StepCounterSensor
import com.fpt.ducna40.masterfit.util.stepcounter.SystemSensor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SensorModule {

    @StepCounterSystemSensor
    @Provides
    @Singleton
    fun providesStepCounterSensor(@ApplicationContext context: Context): SystemSensor = StepCounterSensor(context)

}