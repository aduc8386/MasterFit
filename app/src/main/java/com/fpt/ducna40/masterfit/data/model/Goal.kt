package com.fpt.ducna40.masterfit.data.model

import java.util.UUID
import kotlin.random.Random

data class Goal(
    val title: String = "",
    val stepsPerDay: Int = 0,
    val targetWeight: Double = 0.0,
    val caloriesBurnPerDay: Double = 0.0,
    val targetDate: String = ""
) {
    companion object {
        fun randomGoal() = Goal(
            title = UUID.randomUUID().toString(),
            stepsPerDay = 500,
            targetWeight = Random.nextDouble(),
            caloriesBurnPerDay = 2500.toDouble(),
            targetDate = "",
        )

    }
}