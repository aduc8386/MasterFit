package com.fpt.ducna40.masterfit.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fpt.ducna40.masterfit.data.ResponseStatus
import com.fpt.ducna40.masterfit.data.model.User
import com.fpt.ducna40.masterfit.domain.usecase.GetCurrentUserUseCase
import com.fpt.ducna40.masterfit.domain.usecase.LoginUseCase
import com.fpt.ducna40.masterfit.domain.usecase.RegisterUseCase
import com.fpt.ducna40.masterfit.domain.usecase.RegisterWithEmailNamePhonePasswordUseCase
import com.fpt.ducna40.masterfit.domain.usecase.SetGoalUseCase
import com.fpt.ducna40.masterfit.util.dispatcherprovider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val registerUseCase: RegisterUseCase,
    private val registerWithEmailNamePhonePasswordUseCase: RegisterWithEmailNamePhonePasswordUseCase,
    private val getCurrentUserUseCase: GetCurrentUserUseCase,
    private val setGoalUseCase: SetGoalUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private var _currentUser = MutableStateFlow<ResponseStatus<User>?>(null)
    val currentUser get() = _currentUser.asStateFlow()

    fun getCurrentUser() {
        viewModelScope.launch(dispatcher.io) {
            Log.d("ducna", "getCurrentUser: on thread ${Thread.currentThread().name}")
            _currentUser.update { ResponseStatus.Loading() }
            getCurrentUserUseCase().collect { response ->
                Log.d("ducna", "getCurrentUser: $response")
                _currentUser.value = response
            }
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch(dispatcher.io) {
            _currentUser.value = ResponseStatus.Loading()
            _currentUser.update {
                loginUseCase(email, password)
            }
        }
    }

    fun registerWithEmailNamePhonePassword(
        email: String,
        fullName: String,
        phoneNumber: String,
        password: String
    ) {
        viewModelScope.launch(dispatcher.io) {
            Log.d(
                "ducna",
                "registerWithEmailNamePhonePassword: on thread ${Thread.currentThread().name}"
            )
            _currentUser.value = ResponseStatus.Loading()
            _currentUser.update {
                registerWithEmailNamePhonePasswordUseCase(email, fullName, phoneNumber, password)
            }
        }
    }

    fun registerUser(
        gender: String,
        dateOfBirth: String,
        height: Double,
        weight: Double
    ) {
        viewModelScope.launch(dispatcher.io) {
            Log.d("ducna", "registerUser: on thread ${Thread.currentThread().name}")
            val registeredUser = _currentUser.value?.data
            _currentUser.value = ResponseStatus.Loading()
            _currentUser.update {
                registerUseCase(
                    registeredUser?.fullName ?: "",
                    registeredUser?.phoneNumber ?: "",
                    registeredUser?.email ?: "",
                    registeredUser?.password ?: "",
                    gender,
                    dateOfBirth,
                    height,
                    weight
                )
            }
        }
    }

    fun setGoal(
        title: String,
        targetDate: String,
        targetWeight: Double,
        stepsPerDay: Int,
        caloriesBurnedPerDay: Double
    ) {
        viewModelScope.launch {
            _currentUser.value = ResponseStatus.Loading()
            _currentUser.update {
                setGoalUseCase(
                    title,
                    targetDate,
                    targetWeight,
                    stepsPerDay,
                    caloriesBurnedPerDay
                )
            }
        }
    }
}