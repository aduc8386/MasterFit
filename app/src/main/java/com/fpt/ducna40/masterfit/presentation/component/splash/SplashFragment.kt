package com.fpt.ducna40.masterfit.presentation.component.splash

import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.databinding.FragmentSplashBinding
import com.fpt.ducna40.masterfit.presentation.MainActivity
import com.fpt.ducna40.masterfit.presentation.MainViewModel
import com.fpt.ducna40.masterfit.presentation.UserViewModel
import com.fpt.ducna40.masterfit.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    private val mainViewModel: MainViewModel by activityViewModels()
    override fun getLayoutId(): Int = R.layout.fragment_splash

    override fun bindView() {
        lifecycleScope.launch {
            (requireActivity() as MainActivity).hideBottomNavigation()
            (requireActivity() as MainActivity).hideLoading()
            delay(2000L)

            mainViewModel.setIsNotFirstLaunch()
            findNavController().navigateUp()

            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
                requireActivity().finish()
            }
        }
    }
}