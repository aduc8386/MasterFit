package com.fpt.ducna40.masterfit.data.model

data class Exercise(
    val name: String,
    val duration: Int,
    val exercises: Int,
    val equipments: List<String>,
    val instructions: String,
    val type: String
)