package com.fpt.ducna40.masterfit.di

import com.fpt.ducna40.masterfit.constant.FirebaseReference
import com.fpt.ducna40.masterfit.data.datasource.remote.firebase.UserDatasource
import com.fpt.ducna40.masterfit.data.datasource.remote.firebase.UserFirebaseDatasource
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FirebaseModule {

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = Firebase.auth

    @Provides
    @Singleton
    fun provideFirebaseDatabase(): FirebaseDatabase = FirebaseDatabase.getInstance()

    @UserDatabaseReferences
    @Provides
    @Singleton
    fun provideUserFirebaseReference(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference(FirebaseReference.USER)

    @Provides
    @Singleton
    fun provideUserFirebaseDatasource(
        firebaseAuth: FirebaseAuth,
        @UserDatabaseReferences databaseReferences: DatabaseReference,
    ): UserDatasource = UserFirebaseDatasource(databaseReferences, firebaseAuth)
}