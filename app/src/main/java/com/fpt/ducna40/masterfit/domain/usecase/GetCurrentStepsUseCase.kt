package com.fpt.ducna40.masterfit.domain.usecase

import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.di.StepCounterSystemSensor
import com.fpt.ducna40.masterfit.util.stepcounter.OnStepChanged
import com.fpt.ducna40.masterfit.util.stepcounter.SystemSensor
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class GetCurrentStepsUseCase @Inject constructor(
    @StepCounterSystemSensor private val stepCounterSensor: SystemSensor,
    private val sharedPreferenceHelper: SharedPreferenceHelper,
) {
    operator fun invoke(): Flow<Float> = callbackFlow {
        stepCounterSensor.startListening()

        val listener = object : OnStepChanged {
            override fun onStepChanged(value: List<Float>) {
                sharedPreferenceHelper.setCurrentSteps(value[0].toInt() - sharedPreferenceHelper.getStepsCounted())

                trySend(value[0] - sharedPreferenceHelper.getStepsCounted())
            }
        }
        stepCounterSensor.setListener(listener)

        awaitClose {
            stepCounterSensor.stopListening()
        }

    }

}