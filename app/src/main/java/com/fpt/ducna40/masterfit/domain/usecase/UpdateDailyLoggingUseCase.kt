package com.fpt.ducna40.masterfit.domain.usecase

import android.util.Log
import androidx.core.app.NotificationCompat
import com.fpt.ducna40.masterfit.R
import com.fpt.ducna40.masterfit.data.datasource.local.preference.SharedPreferenceHelper
import com.fpt.ducna40.masterfit.data.model.WorkoutLogging
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

class UpdateDailyLoggingUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(): WorkoutLogging? {

        return if (sharedPreferenceHelper.getIsLoggedIn()) {
            val currentSteps = sharedPreferenceHelper.getCurrentSteps()
            sharedPreferenceHelper.run {
                setCurrentSteps(0)
                setStepsCounted(sharedPreferenceHelper.getStepsCounted() + currentSteps)
            }

            val id = System.currentTimeMillis()

            val currentWeekDay = SimpleDateFormat("EEE", Locale.US).format(Date(id))

            val workoutLogging = WorkoutLogging(
                id = id.toString(),
                currentStep = currentSteps,
                currentCalories = currentSteps * 0.04,
                date = currentWeekDay,
                height = 0.0,
                weight = 0.0,
            )

            Log.d("ducna", "invoke: called $workoutLogging")

            userRepository.updateLogging(workoutLogging)

            workoutLogging

        } else {
            null
        }
    }

}