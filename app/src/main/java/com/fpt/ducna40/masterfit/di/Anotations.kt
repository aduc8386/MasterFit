package com.fpt.ducna40.masterfit.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class UserDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class WorkoutLoggingDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class GoalDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class StepCounterSystemSensor