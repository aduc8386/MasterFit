package com.fpt.ducna40.masterfit.di

import com.fpt.ducna40.masterfit.data.datasource.remote.api.ExerciseDatasource
import com.fpt.ducna40.masterfit.data.datasource.remote.firebase.UserDatasource
import com.fpt.ducna40.masterfit.data.repo.ExerciseMockApiRepository
import com.fpt.ducna40.masterfit.data.repo.UserFirebaseRepository
import com.fpt.ducna40.masterfit.domain.repo.ExerciseRepository
import com.fpt.ducna40.masterfit.domain.repo.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun providesUserRepository(
        userDatasource: UserDatasource
    ): UserRepository {
        return UserFirebaseRepository(userDatasource)
    }

    @Provides
    @Singleton
    fun providesExerciseRepository(
        exerciseDatasource: ExerciseDatasource
    ): ExerciseRepository {
        return ExerciseMockApiRepository(exerciseDatasource)
    }

}